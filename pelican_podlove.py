from pelican import signals

import os.path
import os
import json
import subprocess
import csv # for ffprobe

class Podcast ( object ):
    def __init__ ( self ):
        pass

def runCmd ( cmd ) :
    #for c in cmd :
    #    print (c, end=" ")
    x = subprocess.check_output ( cmd )
    return x.decode("utf-8")

class PodloveGenerator ( object ) :
    def __init__(self, context, settings, path, theme, output_path, *null):
        self.output_path = output_path
        self.context = context
        self.settings = settings
        self.siteurl = settings.get('SITEURL')
        self.relative_urls = settings.get('RELATIVE_URLS')
        if self.relative_urls == True :
            self.siteurl = ""
        self.output_path = output_path
        self.keep = settings.get("PODCAST_KEEP") or False
        self.json_path = settings.get("PODLOVE_JSON_PATH") or "static/js"
        self.ext = ["mp3", "m4a", "ogg", "flac"]

    def findTranscripts ( self, filename ):
        pass

    def castAudio ( self, filename, ext ):
        a = {}
        if not os.path.isfile ( filename ) :
            return None
        a["size"] = os.stat( filename ).st_size;
        a["url"] = self.siteurl + "/" + filename;
        if ext == "mp3":
            a["title"] = "MP3 Audio"
            a["mimeType"] = "audio/mpeg"
        elif ext == "m4a":
            a["title"] = "MP4 Audio"
            a["mimeType"] = "audio/mp4"
        return a

    def findCastAudio ( self, filename ):
        ax = []
        ex = filename.split(".")
        ext = ex[-1]
        a = self.castAudio ( filename, ext )
        if a != None:
            ax = [ a ]

        fn = ""
        for f in ex:
            if f != ext:
                fn += f + "."

        for e in self.ext:
            a = self.castAudio ( filename, ext )
            if a != None:
                ax += [ a ]


        return ax

    def findCastChapters ( self, filename ):
        chapters = []
        cmd = ["ffprobe", "-i", filename, "-show_chapters", "-v", "quiet", "-of", "csv=p=0", "-sexagesimal" ]
        x = runCmd ( cmd );
        x = x.split("\n")
        c = csv.DictReader ( x, fieldnames=["id", "timebase", "start", "starttime", "end", "endtime", "title"] )
        index = 0
        for row in c:
            if index != int(row["id"]) :
                print ("> CHAPTERS ARE OUT-OF-ORDER")
            index+=1
            starttime = row["starttime"]
            st = starttime.split(".")[0]
            ch = {
                "start": st,
                "title": row["title"],
                "href": "",
                "image": "",
            }
            chapters += [ ch ]

        return chapters

    def findCastDuration ( self, filename ):
        cmd = ["ffprobe", "-i", filename, "-show_entries", "format=duration", "-v", "quiet", "-of", "csv=p=0", "-sexagesimal"]
        dur = runCmd ( cmd ).strip().split(".")
        return dur[0]

    def findCast ( self, filename ):
        cast = Podcast()
        cast.audio = self.findCastAudio ( filename )
        if self.settings.get("PODCAST_DOWNLOAD") == True:
            cast.files = cast.audio
        else:
            cast.files = []

        cast.chapters = self.findCastChapters ( filename )
        cast.duration = self.findCastDuration ( filename )
        return cast

    def findContributors ( self, article ):
        contrib = []
        authors = article.podcast_authors if hasattr(article, "podcast_authors") else ""
        if authors == "" :
            authors = article.authors if hasattr(article, "authors") else ""
        if authors == "" :
            authors = article.author if hasattr(article, "author") else ""

        ax = authors
        if type(authors) is str :
            ax = authors.split(",")
        else:
            ax = []
            for a in authors:
                ax += [ a.name ]
        index = 0;
        for author in ax:
            o = { "id": index,
                  "name": author.strip(),
                  "avatar": "/static/avatar/" + author.strip().replace(" ", "-").lower() + ".jpg"
                }
            contrib += [ o ]

        return contrib

    def generate_episode_json ( self, article, filename ):
        print ("> Podlove: Generating ", filename )
        cast = self.findCast ( article.podcast );
        obj = { 
            "version" : 5,
            # global information about the show
            "show": {
                "title": self.settings.get("SITENAME"),
                "subtitle": self.settings.get("SITESUBTITLE"),
                "summary":self.settings.get("SITEDESCRIPTION"),
                "poster":self.settings.get("SITEPOSTER"),
                "link":self.settings.get("SITEURL"),
            },
            ## information about this episode
            "title": article.title,
            "subtitle": "",
            "summary": article.content,  ## remember to maybe shorten it?
            "publicationDate": article.date.isoformat(),
            "duration": cast.duration,
            "poster": article.thumbnail if hasattr(article, "thumbnail") else "",
            "link": self.siteurl + "/" + article.url,
            ## chapter information
            "chapters": cast.chapters,
            ## audio files for playing
            "audio": cast.audio,
            ## files for downloading
            "files": cast.files,
            ## contributors
            "contributors": self.findContributors ( article ),
            "transcripts": self.findTranscripts ( article ),
        }

        js = json.dumps ( obj, separators=(',', ':') )
        os.makedirs ( os.path.dirname ( filename ), exist_ok=True )
        with open(filename, "w", encoding="utf-8") as fd:
            fd.write(js);

    def generate_output ( self, writer ):
        path = os.path.join ( self.output_path, self.json_path );
        for article in self.context["articles"]:
            if hasattr(article, "podcast"):
                filename = os.path.join ( path, article.slug + ".json" );
                if self.keep == False or os.path.isfile ( filename ) == False:
                    self.generate_episode_json ( article, filename );

        return ""

def add_generator( dc ):
    return PodloveGenerator

def register():
    signals.get_generators.connect(add_generator)
